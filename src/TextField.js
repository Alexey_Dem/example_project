import React from 'react'
import Style from "./Style";

const TextField = ({name, onChange, onBlur, error, label}) => (
  <div Style={Style.inputGroup}>
    <label>
      {label}
      <input
        Style={Style.input}
        type="text"
        name={name}
        onChange={onChange}
        onBlur={onBlur}
      />
      {error && <div Style={Style.error}>{error}</div>}
    </label>
  </div>
);

export default TextField;